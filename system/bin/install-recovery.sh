#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/bootdevice/by-name/recovery:55514446:3ae9b269a4e0983f0154a500b35e875293c38aec; then
  applypatch -b /system/etc/recovery-resource.dat EMMC:/dev/block/bootdevice/by-name/boot:50648394:c79e9fcec21ef4cd50e5a981c14e14f98d34e33d EMMC:/dev/block/bootdevice/by-name/recovery 3ae9b269a4e0983f0154a500b35e875293c38aec 55514446 c79e9fcec21ef4cd50e5a981c14e14f98d34e33d:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
